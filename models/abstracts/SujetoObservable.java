package lupin.models.abstracts;
public interface SujetoObservable {
    public void notificar();
}