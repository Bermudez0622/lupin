package lupin.models.abstracts;

public interface Observador {

    public void update();
    
}